API GundalaApp

# How to run
$ composer update
<br/>
$ composer run

# Setup all table
Set database connection string in config.ini file then hit this url http://localhost:8080/api/v1/db/setup

# Remove all table 
Hit this url http://localhost:8080/api/v1/db/setdown
