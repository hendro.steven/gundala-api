<?php

 class SurveyorServices extends BaseServices{

    private $cabang;
    private $account;

    function __construct(){
        parent::__construct(new Surveyor());       
        $this->cabang = new Cabang();
        $this->account = new Account();
    }
  
    function create($params){
        $surveyor = $this->model;
        $this->cabang->load(array('_id = ?',$params['Cabang']));
       
        //create account first
        $this->account->email = $params['Email'];
        $this->account->phone = $params['Phone'];
        $this->account->password = PasswordHash::hashing($params['Password']);
        $this->account->full_name = $params['Nama Lengkap'];
        $this->account->role = 'SURVEYOR';
        $this->account->level = null;
        $this->account->created_date = date('Y-m-d H:i:s');
        $this->account->updated_date = date('Y-m-d H:i:s');
        $this->account->save();

        $surveyor->name = $params['Nama Lengkap'];
        $surveyor->cabang = $this->cabang;
        $surveyor->account = $this->account;
        $surveyor->created_date = date('Y-m-d H:i:s');
        $surveyor->updated_date = date('Y-m-d H:i:s');
        $surveyor->save();
        return $surveyor->cast();
    }

    function update($params){
        $surveyor = $this->model;
        $surveyor->load(array('_id = ?', $params['Id']));
        $this->cabang->load(array('_id = ?',$params['Cabang']));
        
        $surveyor->name = $params['Nama Lengkap'];
        $surveyor->cabang = $this->cabang;
        $surveyor->updated_date = date('Y-m-d H:i:s');
        $surveyor->save();
        return $surveyor->cast();
    }

    function findByName($name, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array('name LIKE ?', "%$name%"),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where name like '%$name%'")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findByCabang($cabang, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array('cabang = ?', $cabang),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where cabang=$cabang")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    public function findByAccount($account){
        $this->model->load(array('account=?',$account));
        return $this->model->cast();
    }

}