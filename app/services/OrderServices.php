<?php

 class OrderServices extends BaseServices{

    private $cabang;
    private $dealer;
    private $account;
    private $surveyor;
    private $status;
    private $statusSvr;
    private $notifSvr;
    private $notifSvr2;
    private $session;

    function __construct(){
        parent::__construct(new Orders());       
        $this->dealer = new Dealer();
        $this->account = new Account();
        $this->cabang = new Cabang();
        $this->surveyor = new Surveyor();
        $this->statusSvr = new OrderStatusServices();
        $this->notifSvr = new NotificationServices();
        $this->notifSvr2 = new NotificationServices();
        $this->session = new Sessions();
    }
  
    function create($params){
        $orders = $this->model;
        $this->session->load(array('sessionId=?',$params['sessionId']));
        $this->dealer->load(array('account = ?',$this->session->account->_id));
        $this->cabang->load(array('_id = ?', $params['Cabang']));

        $orders->number = round(microtime(true) * 1000);
        $orders->cabang = $this->cabang;
        $orders->dealer = $this->dealer;
        $orders->customer_name = $params['Nama Customer'];
        $orders->customer_phone = $params['Telp Customer'];
        $orders->customer_address = $params['Alamat Customer'];
        $orders->source = $params['Source'];
        $orders->jenis_motor = $params['Jenis Motor'];      
        $orders->current_status = 'Baru';
        $orders->save();
        $name = $this->dealer->name;
        $params = array(
            "Orders" => $orders,
            "Status_by" => $this->session->account,
            "Status" => "Baru",
            "Descriptions" => "$name membuat 1 order baru"
        );
        $this->statusSvr->add($params);
        return $orders->cast();
    }

    function update($params){
        $orders = $this->model;
        $orders->load(array('_id = ?', $params['Id']));
     
        $orders->customer_name = $params['Nama Customer'];
        $orders->customer_phone = $params['Telp Customer'];
        $orders->customer_address = $params['Alamat Customer'];
        $orders->source = $params['Source'];
        $orders->jenis_motor = $params['Jenis Motor'];

        $orders->save();
        return $orders->cast();
    }

    function assignSurveyor($params){
        $orders = $this->model;
        $orders->load(array('_id = ?', $params['Id']));
        $this->surveyor->load(array('_id = ?',$params['Survey By']));
        $orders->survey_by = $this->surveyor;
        $orders->current_status = 'Menunggu Disurvei';
        $orders->save();

        //add history status
        $this->session->load(array('sessionId=?',$params['sessionId']));
        $number = $orders->number;
        $name = $this->surveyor->name;
        $params = array(
            "Orders" => $orders,
            "Status_by" => $this->session->account,
            "Status" => "Menunggu Disurvei",
            "Descriptions" => "Order $number akan disurvei oleh $name"
        );
        $this->statusSvr->add($params);

        //notif dealer
        $accDealer = new Account();
        $accDealer->load(array('_id = ?',$orders->dealer->account->_id));
        $paramNotifDealer = array(
            'messages'=> "Order $number akan disurvei oleh $name",
            'accountId'=> $accDealer->_id,
            'orderId'=> $orders->_id
        );  
        $this->notifSvr->sendPushNotification($paramNotifDealer);

        //notif surveyor
        $accSurveyor = $this->surveyor->account;
        $paramNotifSurveyor = array(
            'messages'=> "Anda menerima order $number untuk disurvei",
            'accountId'=> $accSurveyor->_id,
            'orderId'=> $orders->_id
        );
        $this->notifSvr2->sendPushNotification($paramNotifSurveyor);

        return $orders->cast();
    }

    function updateStatus($params){
        $orders = $this->model;
        $orders->load(array('_id = ?', $params['Id']));
        $orders->current_status = $params['Status'];
        $orders->save();

        //add history status
        $this->account->load(array('_id = ?',$params['Updated By']));
        $status = $params['Status'];
        $number = $orders->number;
        $description = $params['Description'];
        $params = array(
            "Orders" => $orders,
            "Status_by" => $this->account,
            "Status" => $status,
            "Descriptions" => $description
        );
        $current_status =  $this->statusSvr->add($params);

        //notif dealer
        $accDealer = new Account();
        $accDealer->load(array('_id = ?',$orders->dealer->account->_id));
        $paramNotifDealer = array(
            'messages'=> "Status Order $number menjadi $status",
            'accountId'=> $accDealer->_id,
            'orderId'=> $orders->_id
        );  
        $this->notifSvr->sendPushNotification($paramNotifDealer);

        //notif surveyor
        $accSurveyor = $orders->survey_by->account;
        $paramNotifSurveyor = array(
            'messages'=> "Status Order $number menjadi $status",
            'accountId'=> $accSurveyor->_id,
            'orderId'=> $orders->_id
        );
        $this->notifSvr2->sendPushNotification($paramNotifSurveyor);

        return $current_status;
    }

    function revertStatus($id){
        return $this->statusSvr->revert($id);
    }

    function findByStatus($status, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("current_status = ?",$status),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where current_status=$status")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;    
    }

    function findByCabang($cabang, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("cabang._id = ?", $cabang),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where cabang=$cabang")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findByCabangAndStatus($cabang, $status, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("cabang._id = ? and current_status = ?", $cabang, $status),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table cabang=$cabang and current_status=$status")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findByDealer($dealer, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("dealer = ?", $dealer),
            array(
                'order'=>'id desc',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where dealer=$dealer")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findByDealerAndStatus($dealer, $status, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("dealer._id = ? and current_status = ?", $dealer),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where dealer=$dealer and current_status=$status")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findByCustomerName($name, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("customer_name like ?", "%$name%"),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where customer_name like '%$name%'")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findByCustomerNameAndDealer($name, $dealer, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("customer_name like ? and dealer = ?", "%$name%", $dealer),
            array(
                'order'=>'id desc',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where customer_name like '%$name%' and dealer=$dealer")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findBySurveyor($surveyor, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("survey_by = ?", $surveyor),
            array(
                'order'=>'id desc',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where survey_by=$surveyor")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findByCustomerNameAndSuveyor($name, $surveyor, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("customer_name like ? and survey_by = ?", "%$name%", $surveyor),
            array(
                'order'=>'id desc',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where customer_name like '%$name%' and survey_by=$surveyor")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findBySurveyorAndStatus($surveyor, $status, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("survey_by._id = ? and current_status = ?", $surveyor, $status),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where survey_by=$surveyor and current_status=$status")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function totalByDealer($dealer){
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['total'] = DB::instance()->exec("select count(*) as _row from $table where dealer=$dealer")[0]['_row'];
        return $result;
    }

    function totalByDealerAndStatus($dealer, $status){
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result = DB::instance()->exec("select count(*) as _row from $table where dealer=$dealer and current_status='$status'")[0]['_row'];
        return $result;
    }

    function totalBySuveyor($surveyor){
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['total'] = DB::instance()->exec("select count(*) as _row from $table where survey_by=$surveyor")[0]['_row'];
        return $result;
    }

    function totalBySurveyorAndStatus($surveyor, $status){
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result = DB::instance()->exec("select count(*) as _row from $table where survey_by=$surveyor and current_status='$status'")[0]['_row'];
        return $result;
    }

}