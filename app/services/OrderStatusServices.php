<?php

 class OrderStatusServices extends BaseServices{


    function __construct(){
        parent::__construct(new OrderStatus());       
    }
  
    function add($params){
        $status = $this->model;
        $status->status_date = date('Y-m-d H:i:s');
        $status->orders = $params['Orders'];
        $status->status_by = $params['Status_by'];
        $status->status_name = $params['Status'];
        $status->descriptions = $params['Descriptions'];
        $status->save();
        return $status->cast();
    }

    function revert($id){
        $status = $this->model;
        $status->load(array("_id = ?", $id));
        $status->erase();
        return true;
    }


}