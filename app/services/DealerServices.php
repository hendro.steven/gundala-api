<?php

 class DealerServices extends BaseServices{

    private $account;

    function __construct(){
        parent::__construct(new Dealer());       
        $this->account = new Account();
    }
  
    function create($params){
        $dealer = $this->model;
       
        //create account first
        $this->account->email = $params['Email'];
        $this->account->phone = $params['Phone'];
        $this->account->password = PasswordHash::hashing($params['Password']);
        $this->account->full_name = $params['Nama Lengkap'];
        $this->account->role = 'DEALER';
        $this->account->level = null;
        $this->account->created_date = date('Y-m-d H:i:s');
        $this->account->updated_date = date('Y-m-d H:i:s');
        $this->account->save();

        $dealer->name = $params['Nama Dealer'];
        $dealer->address = $params['Alamat'];
        $dealer->account = $this->account;
        $dealer->created_date = date('Y-m-d H:i:s');
        $dealer->updated_date = date('Y-m-d H:i:s');
        $dealer->save();
        return $dealer->cast();
    }

    function update($params){
        $dealer = $this->model;
        $dealer->load(array('_id = ?', $params['Id']));
 
        $dealer->name = $params['Nama Dealer'];
        $dealer->address = $params['Alamat'];
        $dealer->updated_date = date('Y-m-d H:i:s');
        $dealer->save();
        return $dealer->cast();
    }

    public function findByAccount($account){
        $this->model->load(array('account=?',$account));
        return $this->model->cast();
    }

    function findByName($name, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array('name LIKE ?', "%$name%"),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where name like '%$name%'")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

}