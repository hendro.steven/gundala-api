<?php

class UserServices extends BaseServices{
  
    private $sessionSvr;
    private $sessionSvrX;

    function __construct(){
        parent::__construct('users'); //set collection name       
        $this->sessionSvr = new SessionServices();
        $this->sessionSvrX = new SessionServices();
    }

    
    function createOne($params){        
        $user = $this->model;
        $user->full_name = $params['Full Name'];     
        $user->phone = $params['Phone'];
        $user->password = PasswordHash::hashing($params['Password']);
        $user->roles = $params['Roles'];      
        $user->save();
        return $this->findOne($user->_id);
    }

    function login($params){
        $user = $this->model;     
        $row = $user->find(array('phone = ?',$params['phone']));   
        $login = array_map(array($user,'cast'),(array)$row,array())[0];
        if($login){
            if(PasswordHash::verifying($params['password'],$login['password'])){
                //invalidate old session
                $this->sessionSvr->invalidate($login['phone']);
                //create new session object       
                return $this->sessionSvrX->createOne($login);
            }
        } 
        return array("login"=>"fail", "message"=>"Invalid phone or password");
    }

}