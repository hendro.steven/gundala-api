<?php

 class AreaServices extends BaseServices{
    private $wilayah;

    function __construct(){
        parent::__construct(new Area());       
        $this->wilayah = new Wilayah();
    }
  
    function create($params){
        $area = $this->model;
        $this->wilayah->load(array('_id = ?',$params['Wilayah']));
        $area->name = $params['Nama'];
        $area->wilayah = $this->wilayah;
        $area->save();
        return $area->cast();
    }

    function update($params){
        $area = $this->model;
        $area->load(array('_id = ?', $params['Id']));
        $this->wilayah->load(array('_id = ?',$params['Wilayah']));
        
        $area->name = $params['Nama'];
        $area->wilayah = $this->wilayah;
        $area->save();
        return $area->cast();
    }

}