<?php

 class CabangServices extends BaseServices{
    private $area;

    function __construct(){
        parent::__construct(new Cabang());       
        $this->area = new Area();
    }
  
    function create($params){
        $cabang = $this->model;
        $this->area->load(array('_id = ?',$params['Area']));
        $cabang->name = $params['Nama'];
        $cabang->area = $this->area;
        $cabang->save();
        return $cabang->cast();
    }

    function update($params){
        $cabang = $this->model;
        $cabang->load(array('_id = ?', $params['Id']));
        $this->area->load(array('_id = ?',$params['Area']));
        
        $cabang->name = $params['Nama'];
        $cabang->area = $this->area;
        $cabang->save();
        return $cabang->cast();
    }

}