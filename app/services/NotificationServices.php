<?php

 class NotificationServices extends BaseServices{

    private $account;

    function __construct(){
        parent::__construct(new Notification());     
        $this->account = new Account();
    }

    function sendPushNotification($params){
        $notif = $this->model;
        $this->account->load(array('_id = ?',$params['accountId']));
        $notif->destination = $params['accountId'];
        $notif->description = $params['messages'];
        $notif->orders = $params['orderId'];
        $notif->sent_date = date('Y-m-d H:i:s');
        $notif->opened = false;
        $notif->save();

		$fields = array(
			'app_id' => "4995bd93-3d00-42ee-bbd7-b0c2acc67fee",
			'include_player_ids' => array($this->account->clientId),
			'data' => array("orderId" => $params['orderId']),
			'contents' => array('en'=> $params['messages'])
		);
		
        $data = json_encode($fields);
        
        $response = \Httpful\Request::post('https://onesignal.com/api/v1/notifications')                
            ->sendsJson()       
            ->addHeaders(array(
                'Content-Type' => 'application/json',              
                'Authorization' => 'Basic NzA4ODNiZWMtOWZjYi00MDA1LTgzYTQtZjdmZWEzNmNhM2Q1',              
             ))                
            ->body($data)             
            ->send();  
		
		return $response->body;
    }
    
    function findByAccountAndStatus($params){
        $page = $params['page'];
        $limit = $params['limit'];
        $account = $params['account'];
        $status = $params['status'];
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array("destination = ? and opened = ?", $account, $status),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where destination=$account and opened=$status")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function totalByAccountAndStatus($account, $status){
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result = DB::instance()->exec("select count(*) as _row from $table where destination=$account and opened=$status")[0]['_row'];
        return $result;
    }

    function openNotification($id){
        $table = preg_replace('/"/','',json_encode($this->model->table));
        DB::instance()->exec("update $table set opened=1 where id=$id");
        return true;
    }
}