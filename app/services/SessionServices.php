<?php

class SessionServices extends BaseServices{

    private $account;

    function __construct(){
        parent::__construct(new Sessions());       
        $this->account = new Account();
    }

    public function findOne($id){
        $this->model->load(array('sessionId=?',$id));
        return $this->model->cast();
    }
    
    function createOne($accountId){        
        $session = $this->model;
        $this->account->load(array('_id = ?',$accountId));
        $session->account = $this->account;
        $session->sessionId =  round(microtime(true) * 1000);
        $session->created_date =  date('Y-m-d H:i:s');
        $session->valid = true;
        $session->save();
        return $session->cast();
    }

    function invalidate($accountId){
        $table = preg_replace('/"/','',json_encode($this->model->table));
        DB::instance()->exec("update $table set valid=false where account=$accountId");
    }

}