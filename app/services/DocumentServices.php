<?php

 class DocumentServices extends BaseServices{


    function __construct(){
        parent::__construct(new OrderDocuments());       
    }
  
    function add($params){
        $attach = $this->model;
        $attach->upload_date = date('Y-m-d H:i:s');
        $attach->orders = $params['Orders'];
        $attach->upload_by = $params['Upload_By'];
        $attach->file_name = $params['File_Name'];
        $attach->title = $params['Title'];
        $attach->deleted = false;
        $attach->save();
        return $attach->cast();
    }

    function delete($id){
        $attach = $this->model;
        $attach->load(array('_id = ?', $id));
        $attach->deleted = true;
        $attach->save();
        return $attach->cast();
    }

    function findByOrder($orderId){
        $rows = $this->model->find(
            array('orders = ? and deleted = ?', $orderId, false),  
            null
        );

        if($rows){
            $result = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result = null;
        }
        return $result;
    }

}