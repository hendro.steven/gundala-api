<?php

 class AppServices extends BaseServices{

    function __construct(){
        parent::__construct(new App());
    }
    
    function create($app_name, $app_domain, $app_token, $exp_time){
        $app = $this->model;
        $app->app_name = $app_name;
        $app->app_domain = $app_domain;
        $app->app_token = $app_token;
        $app->valid_until = date('Y-m-d H:i:s', $exp_time);
        $app->created_date = date('Y-m-d H:i:s');
        $app->updated_date = date('Y-m-d H:i:s');
        $app->save();
        return $app->cast();
    }

}