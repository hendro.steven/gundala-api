<?php

 class AdminServices extends BaseServices{

    private $cabang;
    private $account;

    function __construct(){
        parent::__construct(new Admin());       
        $this->cabang = new Cabang();
        $this->account = new Account();
    }
  
    function create($params){
        $admin = $this->model;
        $this->cabang->load(array('_id = ?',$params['Cabang']));
       
        //create account first
        $this->account->email = $params['Email'];
        $this->account->phone = $params['Phone'];
        $this->account->password = PasswordHash::hashing($params['Password']);
        $this->account->full_name = $params['Nama Lengkap'];
        $this->account->role = 'ADMIN';
        $this->account->level = 'CABANG';
        $this->account->created_date = date('Y-m-d H:i:s');
        $this->account->updated_date = date('Y-m-d H:i:s');
        $this->account->save();

        $admin->name = $params['Nama Lengkap'];
        $admin->cabang = $this->cabang;
        $admin->account = $this->account;
        $admin->created_date = date('Y-m-d H:i:s');
        $admin->updated_date = date('Y-m-d H:i:s');
        $admin->save();
        return $admin->cast();
    }

    function update($params){
        $admin = $this->model;
        $admin->load(array('_id = ?', $params['Id']));
        $this->cabang->load(array('_id = ?',$params['Cabang']));
        
        $admin->name = $params['Nama Lengkap'];
        $admin->cabang = $this->cabang;
        $admin->updated_date = date('Y-m-d H:i:s');
        $admin->save();
        return $admin->cast();
    }

    function findByName($name, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array('name LIKE ?', "%$name%"),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where name like '%$name%'")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    function findByCabang($cabang, $page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            array('cabang = ?', $cabang),
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table where cabang=$cabang")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

    public function findByAccount($account){
        $this->model->load(array('account=?',$account));
        return $this->model->cast();
    }

}