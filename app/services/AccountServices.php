<?php

 class AccountServices extends BaseServices{

    private $sessionSvr;

    function __construct(){
        parent::__construct(new Account());      
        $this->sessionSvr = new SessionServices(); 
    }
  
    function create($params){
        $acc = $this->model;
        $acc->email = $params['Email'];
        $acc->phone = $params['Phone'];
        $acc->password = PasswordHash::hashing($params['Password']);
        $acc->full_name = $params['Nama Lengkap'];
        $acc->role = $params['Role'];
        $acc->level = $params['Level'];
        $acc->created_date = date('Y-m-d H:i:s');
        $acc->updated_date = date('Y-m-d H:i:s');
        $acc->save();
        return $acc->cast();
    }

    function update($params){
        $acc = $this->model;
        $acc->load(array('_id = ?', $params['Id']));
        $acc->email = $params['Email'];
        $acc->phone = $params['Phone'];
        $acc->full_name = $params['Nama Lengkap'];
        $acc->role = $params['Role'];
        $acc->level = $params['Level'];
        $acc->updated_date = date('Y-m-d H:i:s');
        $acc->save();
        return $acc->cast();
    }

    function updateClientId($params){
        $acc = $this->model;
        $acc->load(array('_id = ?', $params['id']));
        $acc->clientId = $params['clientId'];
        $acc->updated_date = date('Y-m-d H:i:s');
        $acc->save();
        return $acc->cast();
    }

    function login($params){
        $acc = $this->model;
        $acc->load(array('email = ?', $params['Email']));
        if(!$acc->dry()){
            if(PasswordHash::verifying($params['Password'],$acc->password)){
                //invalidate old session
                $this->sessionSvr->invalidate($acc->_id);
                //create new session object       
                return $this->sessionSvr->createOne($acc->_id);
            }
        }
        return array("login"=>"fail", "message"=>"Invalid email or password");
    }

}