<?php

 class WilayahServices extends BaseServices{

    function __construct(){
        parent::__construct(new Wilayah());       
    }
  
    function create($params){
        $wilayah = $this->model;
        $wilayah->name = $params['Nama'];
        $wilayah->save();
        return $wilayah->cast();
    }

    function update($params){
        $wilayah = $this->model;
        $wilayah->load(array('_id = ?', $params['Id']));
        $wilayah->name = $params['Nama'];       
        $wilayah->save();
        return $wilayah->cast();
    }

}