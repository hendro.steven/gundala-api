<?php


class LoginController  extends BaseRoute {

    private $accountSvr;

    function __construct(){
        parent::__construct();
        $this->accountSvr = new AccountServices();
    }

    function login(){
        $params = array(
            "Email" => $this->post['email'],
            "Password" => $this->post['password']
        );

        $this->data = array(
            'status' => true,
            'data' => $this->accountSvr->login($params)
        );
    }
    
}