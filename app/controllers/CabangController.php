<?php

class CabangController extends SecureRoute {

    private $cabangSvr;

    function __construct(){
        parent::__construct(true);
        $this->cabangSvr = new CabangServices();
    }

    function create(){
        $params = array(
            'Nama' => $this->post['nama'],
            'Area' => $this->post['area']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Nama', 'Area']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->cabangSvr->create($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function update(){
        $id = $this->params['id'];
        $params = array(
            'Id' => $id,
            'Nama' => $this->post['nama'],
            'Area' => $this->post['area']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Id','Nama','Area']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->cabangSvr->update($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function findOne(){
        $id = $this->params['id'];
        $this->data = [
            'status' => true,
            'data' => $this->cabangSvr->findOne($id)
        ];
    }

    function findAll(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $results = $this->cabangSvr->findAll($page,$limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

}