<?php

class OrderController extends SecureRoute {

    private $orderSvr;

    function __construct(){
        parent::__construct(true);
        $this->orderSvr = new OrderServices();
    }

    function create(){
        $params = array(
            'sessionId' => $this->post['sessionId'],
            'Nama Customer' => $this->post['customer'],
            'Telp Customer' => $this->post['telp'],
            'Alamat Customer' => $this->post['address'],
            'Source' => $this->post['source'],
            'Jenis Motor' => $this->post['jenis'],
            'Cabang' => $this->post['cabang']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Nama Customer','Telp Customer','Alamat Customer','Source', 'Jenis Motor', 'Cabang']);
       
         //check if valid
         if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->orderSvr->create($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function update(){
        $params = array(
            'Id' => $this->params['id'],
            'Nama Customer' => $this->post['customer'],
            'Telp Customer' => $this->post['telp'],
            'Alamat Customer' => $this->post['address'],
            'Source' => $this->post['source'],
            'Jenis Motor' => $this->post['jenis']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Id','Nama Customer','Telp Customer','Alamat Customer','Source', 'Jenis Motor']);
       
         //check if valid
         if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->orderSvr->update($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function assignSurveyor(){
        $params = array(
            'Id' => $this->params['id'],
            'Survey By' => $this->post['surveyor'],
            'sessionId' => $this->post['sessionId']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Id','Survey By']);
       
         //check if valid
         if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->orderSvr->assignSurveyor($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function updateStatus(){
        $params = array(
            'Id' => $this->params['id'],
            'Updated By'=> $this->post['updated_by'],
            'Status' => $this->post['status'],
            'Description' => $this->post['description']
        );    
         //put validation rule
         $v = new Valitron\Validator($params);
         $v->rule('required', ['Id','Updated By','Status','Description']);
        
          //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->orderSvr->updateStatus($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function findOne(){
        $id = $this->params['id'];
        $this->data = [
            'status' => true,
            'data' => $this->orderSvr->findOne($id)
        ];
    }

    function findAll(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $results = $this->orderSvr->findAll($page,$limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function findByDealer(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $dealer = $this->post['dealer'];
        $results = $this->orderSvr->findByDealer($dealer, $page, $limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function findByCustomerNameAndDealer(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $dealer = $this->post['dealer'];
        $name = $this->post['name'];
        $results = $this->orderSvr->findByCustomerNameAndDealer($name, $dealer, $page, $limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function findBySurveyor(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $surveyor = $this->post['surveyor'];
        $results = $this->orderSvr->findBySurveyor($surveyor, $page, $limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function findByCustomerNameAndSurveyor(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $surveyor = $this->post['surveyor'];
        $name = $this->post['name'];
        $results = $this->orderSvr->findByCustomerNameAndSuveyor($name, $surveyor, $page, $limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function totalByDealerAndStatus(){
        $dealer = $this->post['dealer'];
        //$status = $this->post['status'];
        $results['Baru'] = $this->orderSvr->totalByDealerAndStatus($dealer, 'Baru');
        $results['Sudah Disurvei'] = $this->orderSvr->totalByDealerAndStatus($dealer, 'Sudah Disurvei');
        $results['Diterima'] = $this->orderSvr->totalByDealerAndStatus($dealer, 'Diterima');
        $results['Ditolak'] = $this->orderSvr->totalByDealerAndStatus($dealer, 'Ditolak');
        $results['Batal'] = $this->orderSvr->totalByDealerAndStatus($dealer, 'Batal');

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function totalBySurveyorAndStatus(){
        $surveyor = $this->post['surveyor'];
        $results['Menunggu Disurvei'] = $this->orderSvr->totalBySurveyorAndStatus($surveyor, 'Menunggu Disurvei');
        $results['Sudah Disurvei'] = $this->orderSvr->totalBySurveyorAndStatus($surveyor, 'Sudah Disurvei');
        $results['Diterima'] = $this->orderSvr->totalBySurveyorAndStatus($surveyor, 'Diterima');
        $results['Ditolak'] = $this->orderSvr->totalBySurveyorAndStatus($surveyor, 'Ditolak');
        $results['Batal'] = $this->orderSvr->totalBySurveyorAndStatus($surveyor, 'Batal');

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

}