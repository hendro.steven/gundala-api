<?php

class DocumentController extends SecureRoute{
    private $documentSvr;

    function __construct(){
        parent::__construct(false);
        $this->documentSvr = new DocumentServices();
    }

    function upload(){
        $params = array(
            'Orders'=> $this->f3->get('POST.orders'),
            'Upload_By' => $this->f3->get('POST.upload_by'),
            'Title' => $this->f3->get('POST.title')
        );    
        $formFieldName = $this->f3->get('POST.dokumen');
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Orders','Upload_By','Title']);
        
        //check if valid
        if ($v->validate()) { //valid
            $web = Web::instance();
            $overwrite = false;
            $files = $web->receive(function($file, $formFieldName) {
                if ($file['size'] > (2 * 1024 * 1024)) { // if bigger than 2 MB
                    $this->data = [
                        'success'=> false, 
                        'payload'=> array("message" => "Photo size is more than 2 Mb")
                    ];
                    return false;
                }            
                return true; // allows the file to be moved from php tmp dir to your defined upload dir
            }, $overwrite, function($fileBaseName, $formFieldName) {
                $name = $this->millitime() . '_' . $fileBaseName;
                $this->f3->set('fileName', $name);
                $this->f3->set('uploaded', true);
                return $name;
            });
            
            if ($this->f3->get('uploaded') == true) {
                $params['File_Name'] = $this->f3->get('fileName');
                $this->data = [
                    'status'=> true, 
                    'data'=> $this->documentSvr->add($params)
                ];
            }else{
                $this->data = [
                    'success'=> false, 
                    'payload'=> array("message" => "There's some errors, please try again")
                ];
            }
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function uploadCloud(){
        $params = array(
            'Orders'=> $this->f3->get('POST.orders'),
            'Upload_By' => $this->f3->get('POST.upload_by'),
            'Descriptions' => $this->f3->get('POST.descriptions'),
            'Title' => $this->f3->get('POST.title')
        );    
        $formFieldName = $this->f3->get('POST.dokumen');

        $v = new Valitron\Validator($params);
        $v->rule('required', ['Application','Upload_By','Title']);
 
        if ($v->validate()) { 
            $web = Web::instance();
            $overwrite = true;
            $files = $web->receive(function($file, $formFieldName) {
                if ($file['size'] > (2 * 1024 * 1024)) { // if bigger than 2 MB
                    $this->data = [
                        'success'=> false, 
                        'payload'=> array("message" => "Photo size is more than 2 Mb")
                    ];
                    return false;
                }            
                return true;
            }, $overwrite, function($fileBaseName, $formFieldName) {
                $name = $this->millitime() . '_' . $fileBaseName;
                $this->f3->set('fileName', $name);
                return $name;
            });

            $fileName = $this->f3->get('fileName');
            $cloudResult = $this->uploadToCloudinary($fileName, $params['Orders'].'-'.$params['Title']);
            $params['File_Name'] = $cloudResult['secure_url'];
            $this->data = [
                'status'=> true, 
                'data'=> $params//$this->documentSvr->add($params)
            ];

        }else{ 
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    private function uploadToCloudinary($fileName,$title){
        Cloudinary::config(array( 
            "cloud_name" => "dblbzbgkg", 
            "api_key" => "127875798375621", 
            "api_secret" => "8nCmKWHoBqAjYh6PZiLCnDb2sd0", 
            "secure" => true
        ));
        $options = array(
            'tags' => $title
        );
        $file = getcwd() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $fileName;
        $result = \Cloudinary\Uploader::upload($file, $options);
        unlink($file);
        return $result;
    }

    private function millitime() {
        $microtime = microtime();
        $comps = explode(' ', $microtime);
        return sprintf('%d%03d', $comps[1], $comps[0] * 1000);
    }

    function delete(){
        $id = $this->params['id'];
        $this->data = [
            'status'=> true, 
            'data'=> $this->documentSvr->delete($id)
        ];
    }

    function findByOrder(){
        $orderId = $this->params['orderId'];
        $this->data = [
            'status' => true,
            'data' => $this->documentSvr->findByOrder($orderId)
        ];
    }
}