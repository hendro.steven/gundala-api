<?php

class WilayahController extends SecureRoute {

    private $wilayahSvr;

    function __construct(){
        parent::__construct(true);
        $this->wilayahSvr = new WilayahServices();
    }

    function create(){
        $params = array(
            'Nama' => $this->post['nama']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Nama']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->wilayahSvr->create($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function update(){
        $id = $this->params['id'];
        $params = array(
            'Id' => $id,
            'Nama' => $this->post['nama']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Id','Nama']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->wilayahSvr->update($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function findOne(){
        $id = $this->params['id'];
        $this->data = [
            'status' => true,
            'data' => $this->wilayahSvr->findOne($id)
        ];
    }

    function findAll(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $results = $this->wilayahSvr->findAll($page,$limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

}