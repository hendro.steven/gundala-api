<?php

class NotificationController extends SecureRoute{

    private $notifSvr;

    function __construct(){
        parent::__construct(false); 
        $this->notifSvr = new NotificationServices();
    }

    function sendPushNotification(){
        $params = array(
            'messages'=> $this->post['messages'],
            'accountId'=> $this->post['accountId'],
            'orderId'=> $this->post['orderId']
        );    
        $this->data = [
            'status'=> true, 
            'data'=> $this->notifSvr->sendPushNotification($params)
        ];
    }

    function findByAccountAndStatus(){
        $params = array(
            'account'=> $this->post['accountId'],
            'status'=> $this->post['status'],
            'page' => $this->params['page'],
            'limit' => $this->params['limit']
        );    
        $this->data = [
            'status'=> true, 
            'data'=> $this->notifSvr->findByAccountAndStatus($params)
        ];
    }

    function totalByAccountAndStatus(){
        $id = $this->params['id'];
        $results = $this->notifSvr->totalByAccountAndStatus($id, 0);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function openNotification(){
        $id = $this->params['id'];
        $results = $this->notifSvr->openNotification($id);
        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }
}