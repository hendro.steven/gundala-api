<?php

class SurveyorController extends SecureRoute {

    private $surveyorSvr;

    function __construct(){
        parent::__construct(true);
        $this->surveyorSvr = new SurveyorServices();
    }

    function create(){
        $params = array(
            'Email'=> $this->post['email'],
            'Phone' => $this->post['phone'],
            'Password' => $this->post['password'],
            'Nama Lengkap' => $this->post['full_name'],
            'Cabang' => $this->post['cabang']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Email','Phone','Password','Nama Lengkap', 'Cabang']);
        $v->rule('email',['Email']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->surveyorSvr->create($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function update(){
        $id = $this->params['id'];
        $params = array(
            'Id' => $id,
            'Nama Lengkap' => $this->post['full_name'],
            'Cabang' => $this->post['cabang'],           
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Id','Nama Lengkap', 'Cabang']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->surveyorSvr->update($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function findOne(){
        $id = $this->params['id'];
        $this->data = [
            'status' => true,
            'data' => $this->surveyorSvr->findOne($id)
        ];
    }

    function findAll(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $results = $this->surveyorSvr->findAll($page,$limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function findByName(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $nama = $this->post['nama'];
        $results = $this->surveyorSvr->findByName($nama, $page,$limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function findByCabang(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $cabang = $this->post['cabang'];
        $results = $this->surveyorSvr->findByCabang($cabang, $page,$limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function findByAccount(){
        $id = $this->post['account'];
        $this->data = [
            'status' => true,
            'data' => $this->surveyorSvr->findByAccount($id)
        ];
    }


}