<?php

class AccountController extends SecureRoute{

    private $accountSvr;

    function __construct(){
        parent::__construct(false); //true mean check sessionId, empty=false
        $this->accountSvr = new AccountServices();
    }

    function create(){
        $acc = array(
            'Email'=> $this->post['email'],
            'Phone' => $this->post['phone'],
            'Password' => $this->post['password'],
            'Nama Lengkap' => $this->post['full_name'],
            'Role' => $this->post['role'],
            'Level' => $this->post['level']
        );    

        //put validation rule
        $v = new Valitron\Validator($acc);
        $v->rule('required', ['Email','Phone','Password','Nama Lengkap','Role']);
        $v->rule('email',['Email']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->accountSvr->create($acc)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function updateClientId(){
        $params = array(
            'id'=> $this->params['id'],
            'clientId' => $this->post['clientId']
        );    
        $this->data = [
            'status'=> true, 
            'data'=> $this->accountSvr->updateClientId($params)
        ];
    }

    function findOne(){
        $id = $this->params['id'];
        $this->data = [
            'status' => true,
            'data' => $this->accountSvr->findOne($id)
        ];
    }

    function findAll(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $accounts = $this->accountSvr->findAll($page,$limit);

        $this->data = [
            'status' => true,
            'data' => $accounts
        ];
    }

   
}