<?php


class SessionController  extends SecureRoute {

    private $sessionSvr;

    function __construct(){
        parent::__construct();
        $this->sessionSvr = new SessionServices();
    }

    function findOne(){
        $id =  $this->params['id'];
      
        $this->data = array(
            'status' => true,
            'data' => $this->sessionSvr->findOne($id)
        );
    }
    
}