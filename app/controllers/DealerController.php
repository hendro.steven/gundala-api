<?php

class DealerController extends SecureRoute {

    private $dealerSvr;

    function __construct(){
        parent::__construct(true);
        $this->dealerSvr = new DealerServices();
    }

    function create(){
        $params = array(
            'Email'=> $this->post['email'],
            'Phone' => $this->post['phone'],
            'Password' => $this->post['password'],
            'Nama Lengkap' => $this->post['full_name'],
            'Nama Dealer' => $this->post['nama_dealer'],
            'Alamat' => $this->post['alamat']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Email','Phone','Password','Nama Lengkap', 'Nama Dealer','Alamat']);
        $v->rule('email',['Email']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->dealerSvr->create($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function update(){
        $id = $this->params['id'];
        $params = array(
            'Id' => $id,          
            'Nama Dealer' => $this->post['nama_dealer'],
            'Alamat' => $this->post['alamat'],            
        );    
        
        //put validation rule
        $v = new Valitron\Validator($params);
        $v->rule('required', ['Id','Nama Dealer','Alamat']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->dealerSvr->update($params)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function findOne(){
        $id = $this->params['id'];
        $this->data = [
            'status' => true,
            'data' => $this->dealerSvr->findOne($id)
        ];
    }

    function findByAccount(){
        $id = $this->post['account'];
        $this->data = [
            'status' => true,
            'data' => $this->dealerSvr->findByAccount($id)
        ];
    }

    function findAll(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $results = $this->dealerSvr->findAll($page,$limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

    function findByName(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $nama = $this->post['nama'];
        $results = $this->dealerSvr->findByName($nama, $page,$limit);

        $this->data = [
            'status' => true,
            'data' => $results
        ];
    }

}