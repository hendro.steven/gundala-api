<?php

class ClientController extends SecureRoute {

    private $clientSvr;

    function __construct(){
        parent::__construct();
        $this->clientSvr = new ClientServices();
    }

    function create(){
        $client = array(
            'Client Code'=> $this->post['code'],
            'Client Name' => $this->post['name'],
            'Status' => $this->post['status']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($client);
        $v->rule('required', ['Client Code','Client Name','Status']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->clientSvr->create($client)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function update(){
        $client = array(
            'Id' => $this->params['id'],
            'Client Code'=> $this->post['code'],
            'Client Name' => $this->post['name'],
            'Status' => $this->post['status']
        );    
        
        //put validation rule
        $v = new Valitron\Validator($client);
        $v->rule('required', ['Client Code','Client Name','Status']);
        
        //check if valid
        if ($v->validate()) { //valid
            $this->data = [
                'status'=> true, 
                'data'=> $this->clientSvr->update($client)
            ];
        }else{ //not valid
            $this->data = [
                'success'=> false, 
                'payload'=> $v->errors()
            ];
        }
    }

    function findAll(){
        $page = $this->params['page'];
        $limit = $this->params['limit'];
        $clients = $this->clientSvr->findAll($page,$limit);

        $this->data = [
            'status' => true,
            'data' => $clients
        ];
    }

}