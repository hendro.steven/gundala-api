<?php

class HomeController  extends BaseRoute {
    function index(){
        $this->data = [
            'success'=> true, 
            'payload'=> 'Welcome to OrderApp API v1.0'
        ];
    }
}