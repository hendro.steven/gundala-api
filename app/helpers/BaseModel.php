<?php

class BaseModel extends \DB\Cortex{

    protected $db;
    public $table; // nama table

    public function __construct($table) {
        $f3 = Base::instance(); 
        $fluid = $f3->get('fluid_mode');
        $this->table = $table;
        if($this->db == null){
            $this->db = DB::instance(); // get database connection
        }
        parent::__construct($this->db, $this->table, $fluid);
    }
}