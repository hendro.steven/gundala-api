<?php

class DB {
    static $conn;

    public static function instance(){
        $f3 = Base::instance(); 
        if($conn == null){
            try{
                $conn = new DB\SQL($f3->get('db_dns') . $f3->get('db_name'), $f3->get('db_user'), $f3->get('db_pass')); 
            }catch(Exception $err){
                var_dump($err);
            }
        }
        return $conn;
    }
}