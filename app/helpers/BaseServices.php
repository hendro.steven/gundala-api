<?php

class BaseServices {

    protected $model;
  
    function __construct($model){
        $this->model = $model;
    }

    public function findOne($id){
        $this->model->load(array('_id=?',$id));
        return $this->model->cast();
    }


    function findAll($page, $limit){
        if($page<1){
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $rows = $this->model->find(
            null,
            array(
                'order'=>'id',
                'offset'=>(int)$offset,
                'limit'=>(int)$limit
            )
        );
        if($rows){
            $result['subset'] = array_map(array($this->model,'cast'),(array)$rows,array());
        }else{
            $result['subset'] = null;
        }
        $table = preg_replace('/"/','',json_encode($this->model->table));
        $result['rows'] = DB::instance()->exec("select count(*) as _row from $table")[0]['_row'];
        $result['limit'] = $limit;
        $result['pages'] = ceil($result['rows']/$limit);
        $result['page'] = $page;
        return $result;
    }

}