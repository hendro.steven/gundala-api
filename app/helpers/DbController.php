<?php

class DbController extends BaseRoute {

    function setup(){
        App::setup();
        Account::setup();
        Wilayah::setup();
        Area::setup();
        Cabang::setup();
        Dealer::setup();
        Surveyor::setup();
        Admin::setup();
        Orders::setup();
        OrderStatus::setup();
        OrderDocuments::setup();
        Sessions::setup();
        Notification::setup();

        $this->data = [
            'status'=> true, 
            'data'=> 'All table setup'
        ];
    }
    

    function setdown(){
        Notification::setdown();
        Sessions::setdown();
        OrderDocuments::setdown();
        OrderStatus::setdown();
        Orders::setdown();
        Admin::setdown();
        Surveyor::setdown();
        Dealer::setdown();
        Cabang::setdown();
        Area::setdown();
        Wilayah::setdown();
        Account::setdown();
        App::setdown();
        
        $this->data = [
            'status'=> true, 
            'data'=> 'All table remove'
        ];
    }

}