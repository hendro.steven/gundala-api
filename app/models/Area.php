<?php

class Area extends BaseModel{

    protected $fieldConf = array(
            'name' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false,
                'unique' => true,
            ), 
            'wilayah' => array(
                'belongs-to-one' => '\Wilayah'
            )
        );

    public function __construct() {
        parent::__construct('tbl_area');
    }
}