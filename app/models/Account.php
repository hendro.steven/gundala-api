<?php

class Account extends BaseModel{

    protected $fieldConf = array(
            'email' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false,
                'unique' => true
            ),
            'phone' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR128,
                'nullable' => false,
                'unique' => true
            ),
            'password' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false
            ), 
            'full_name' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false
            ),
            //DEALER, SURVEYOR, ADMIN, SUPER_ADMIN
            'role' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR128,
                'nullable' => false
            ),
            //CABANG, AREA, WILAYAH
            'level' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR128,
                'nullable' => true
            ),
            'clientId' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR128,
                'nullable' => true
            ),
            'created_date' => array(
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false
            ),
            'updated_date' => array(
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false
            ),
        );

    public function __construct() {
        parent::__construct('tbl_account');
    }
}