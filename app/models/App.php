<?php

class App extends BaseModel{
    
    protected $fieldConf = array(
        'app_name' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
            'nullable' => false,
            'unique' => true,
        ),
        'app_domain' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
            'nullable' => false,
            'unique' => true,
        ),
        'app_token' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
            'nullable' => false,
            'unique' => true
        ),
        'valid_until' => array(
            'type' => \DB\SQL\Schema::DT_DATETIME,
            'nullable' => false
        ),
        'created_date' => array(
            'type' => \DB\SQL\Schema::DT_DATETIME,
            'nullable' => false
        ),
        'updated_date' => array(
            'type' => \DB\SQL\Schema::DT_DATETIME,
            'nullable' => false
        ),
    );

    public function __construct() {
        parent::__construct('tbl_app');
    }
}