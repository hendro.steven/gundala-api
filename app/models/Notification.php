<?php

class Notification extends BaseModel{

    protected $fieldConf = array(
            'destination' => array(
                'belongs-to-one' => '\Account'
            ),
            'orders'=> array(
                'belongs-to-one' => '\Orders'
            ),
            'description' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false,
                'unique' => false,
            ), 
            'sent_date' => array(
                'type' => \DB\SQL\Schema::DT_DATETIME,
            ),
            'opened' => array(
                'type' => \DB\SQL\Schema::DT_BOOLEAN,
            )
        );

    public function __construct() {
        parent::__construct('tbl_notification');
    }
}