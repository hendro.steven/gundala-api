<?php

class Surveyor extends BaseModel{

    protected $fieldConf = array(           
            'name' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false,
                'unique' => false,
            ), 
            'cabang' => array(
                'belongs-to-one' => '\Cabang'
            ),
            'account' => array(
                'belongs-to-one' => '\Account'
            ),
            'created_date' => array(
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false
            ),
            'updated_date' => array(
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false
            ),
        );

    public function __construct() {
        parent::__construct('tbl_surveyor');
    }
}