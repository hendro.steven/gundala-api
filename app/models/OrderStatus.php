<?php

class OrderStatus extends BaseModel{

    protected $fieldConf = array(
            'status_date' => array(
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false
            ),
            'orders' => array(
                'belongs-to-one' => '\Orders'
            ),
            'status_by' => array(
                'belongs-to-one' => '\Account'
            ),
            'status_name' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR128,
                'nullable' => false
            ), 
            'descriptions' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => true
            )
        );

    public function __construct() {
        parent::__construct('tbl_order_status');
    }
}