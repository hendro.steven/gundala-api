<?php

class OrderDocuments extends BaseModel{

    protected $fieldConf = array(
            'upload_date' => array(
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false
            ),
            'orders' => array(
                'belongs-to-one' => '\Orders'
            ),
            'upload_by' => array(
                'belongs-to-one' => '\Account'
            ),
            'file_name' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false,
                'unique' => true,
            ), 
            'title' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => true
            ),
            'deleted' => array(
                'type' => \DB\SQL\Schema::DT_BOOLEAN,
            )
        );

    public function __construct() {
        parent::__construct('tbl_order_documents');
    }
}