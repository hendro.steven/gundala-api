<?php

class Orders extends BaseModel{

    protected $fieldConf = array(
            'number' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR128,
                'nullable' => false,
                'unique' => true,
            ), 
            'cabang' => array(
                'belongs-to-one' => '\Cabang'
            ),
            'dealer' => array(
                'belongs-to-one' => '\Dealer'
            ),
            'customer_name' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false
            ), 
            'customer_phone' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false,
                'unique' => true,
            ), 
            'customer_address' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false
            ), 
            'source' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR128,
                'nullable' => false
            ), 
            'jenis_motor' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR128,
                'nullable' => false
            ), 
            'survey_by' => array(
                'belongs-to-one' => '\Surveyor'
            ),
            'current_status' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR128,
                'nullable' => false
            ),
        );

    public function __construct() {
        parent::__construct('tbl_orders');
    }
}