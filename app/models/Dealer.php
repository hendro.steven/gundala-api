<?php

class Dealer extends BaseModel{

    protected $fieldConf = array(         
            'name' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false,
                'unique' => false,
            ), 
            'address' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false,
                'unique' => false,
            ), 
            'account' => array(
                'belongs-to-one' => '\Account'
            ),
            'created_date' => array(
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false
            ),
            'updated_date' => array(
                'type' => \DB\SQL\Schema::DT_DATETIME,
                'nullable' => false
            ),
        );

    public function __construct() {
        parent::__construct('tbl_dealer');
    }
}