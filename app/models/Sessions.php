<?php

class Sessions extends BaseModel{

    protected $fieldConf = array(
        'account' => array(
            'belongs-to-one' => '\Account'
        ),
        'sessionId' => array(
            'type' => \DB\SQL\Schema::DT_VARCHAR256,
            'nullable' => false,
            'unique' => true
        ),
        'created_date' => array(
            'type' => \DB\SQL\Schema::DT_DATETIME,
            'nullable' => false
        ),
        'valid' => array(
            'type' => \DB\SQL\Schema::DT_BOOLEAN,
            'nullable' => false
        )
    );

    public function __construct() {
        parent::__construct('tbl_sessions');
    }
}