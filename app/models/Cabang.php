<?php

class Cabang extends BaseModel{

    protected $fieldConf = array(
            'name' => array(
                'type' => \DB\SQL\Schema::DT_VARCHAR256,
                'nullable' => false,
                'unique' => true,
            ), 
            'area' => array(
                'belongs-to-one' => '\Area'
            )
        );

    public function __construct() {
        parent::__construct('tbl_cabang');
    }
}